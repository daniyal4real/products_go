package apiserver // Определение пакета apiserver.

type Config struct {
	Port string // Поле Port в структуре Config для хранения номера порта.
	LogLevel string // Поле LogLevel для определения уровня логирования
}

func NewConfig(port, logLevel string) *Config { // Функция NewConfig создает новый экземпляр Config.
	return &Config{
		Port: port, // Вставляет зачение в поле Port из port который принимает
		LogLevel: logLevel, // Вставляет зачение в поле LogLevel из logLevel который принимает
	} // Возвращает указатель на новый экземпляр Config.
}



