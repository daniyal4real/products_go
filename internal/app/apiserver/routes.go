package apiserver

import (
	"net/http"
	"github.com/gorilla/mux"
)


func (s *APIServer) configureRouter() {
	r := mux.NewRouter()
    r.HandleFunc("/test", s.TestHandler)
	http.Handle("/", r)
}