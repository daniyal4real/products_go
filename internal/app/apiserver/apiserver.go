package apiserver

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

type APIServer struct {
	config *Config        // Поле конфиг для конфигураций
	logger *logrus.Logger // Поле с логгером logrus
	router *mux.Router    // Поле для роутера
}

func New(config *Config) *APIServer { // Функция New() создает новый экземпляр APIServer.
	return &APIServer{
		config: config, // сохраняем значение config
		logger: logrus.New(),
		router: mux.NewRouter(),
	} // Возвращает указатель на новый экземпляр APIServer.
}

func (s *APIServer) Start() error { // Метод Start() запускает сервер API.
	if err := s.configureLogger(); err != nil {
		return err
	}

	s.configureRouter()

	s.logger.Info("Сервер запущен на порту", s.config.Port) // INFO лог о запуске сервера и port
	return http.ListenAndServe(s.config.Port, s.router)
}

func (s *APIServer) configureLogger() error { // конфигурация логгера logrus
	level, err := logrus.ParseLevel(s.config.LogLevel)
	if err != nil {
		return err
	}
	// установка уровня логирования на значение LogLevel
	s.logger.SetLevel(level)
	return nil
}







