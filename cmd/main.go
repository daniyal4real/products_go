package main

import (
	"log"
	"os"
	"rest_api/internal/app/apiserver"
	"github.com/joho/godotenv"
)

func main() { 
	godotenv.Load("../.env") // Прописываем путь к .env для загрузки

	config := apiserver.NewConfig(os.Getenv("PORT"), os.Getenv("LOG_LEVEL"))
	server := apiserver.New(config)                  // Создаем новый экземпляр сервера API с конфигом.

	if err := server.Start(); err != nil { // Если возникла ошибка при запуске сервера,
		log.Fatal("Ошибка при запуске сервера:", err) // записываем ошибку в лог и завершаем программу.
	}
}

















