module rest_api

go 1.20

require (
	github.com/joho/godotenv v1.5.1
	github.com/sirupsen/logrus v1.9.3
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8
)

require github.com/gorilla/mux v1.8.0 // indirect
